module PathgraphEncoding
  # Pathgraph encoding library version
  VERSION = "0.0.3"

  # Minimum hypercube degree
  MIN_Q_N = 4
end
