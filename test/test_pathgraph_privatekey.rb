require 'minitest/autorun'
require 'pathgraph_encoding'

class TestPathGraphPrivateKey < Minitest::Test

  def setup
    @private_key = {
      n: 4,
      pi: [[0,1,3,2],[6,7,5,4],[12,13,15,14],[10,11,9,8]]
    }
    @der_private_key = "0F\x13\x050.0.10=\x02\x01\x04080\f\x02\x01\x00"\
    "\x02\x01\x01\x02\x01\x03\x02\x01\x020\f\x02\x01\x06\x02\x01\a\x02"\
    "\x01\x05\x02\x01\x040\f\x02\x01\f\x02\x01\r\x02\x01\x0F\x02\x01"\
    "\x0E0\f\x02\x01\n\x02\x01\v\x02\x01\t\x02\x01\b"
  end

  def test_to_der
    der = PathgraphEncoding::PrivateKey::to_der(@private_key)
    assert_equal 72, der.size
    assert_equal @der_private_key, der
  end

  def test_from_der
    der = "0*\x13\x050.0.10!\x02\x01\x040\x1C0\f\x02\x01\x01\x02\x01\x02\x02\x01\x03\x02\x01\x040\f\x02\x01\x05\x02\x01\x06\x02\x01\a\x02\x01\b"
    key = PathgraphEncoding::PrivateKey::from_der(der)
    exp = {version: "0.0.1", n: 4, pi: [[1, 2, 3, 4], [5, 6, 7, 8]]}
    assert_equal key, exp
  end

  def test_is_valid
    skip "TODO: implement 'is_valid' testcase"
  end

  def test_is_valid_hypercube_degree
    (4..8).each do |i|
      assert PathgraphEncoding::PrivateKey::is_valid_hypercube_degree?(i)
    end
  end

  def test_is_adyacent_in_hp
    skip "TODO: implement 'is_adyacent_in_hp' testcase"
  end

  def test_gen_public_key
    expected = {
      n: 4,
      sigma: [[0,2],[6,4],[12,14],[10,8]]
    }
    key = PathgraphEncoding::PrivateKey.gen_public_key(@private_key)
    assert_equal expected, key
    refute_same @private_key, key
    refute_same @private_key[:pi], key[:sigma]
  end

end