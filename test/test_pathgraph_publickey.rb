require 'minitest/autorun'
require 'pathgraph_encoding'

class TestPathGraphPublicKey < Minitest::Test

  def setup
    @public_key = {
      n: 4,
      sigma: [[0,2],[6,4],[12,14],[10,8]]
    }
    @der_public_key = "0.\x13\x05#{PathgraphEncoding::VERSION}0%"\
    "\x02\x01\x040 0\x06\x02\x01\x00\x02\x01\x020\x06\x02\x01\x06"\
    "\x02\x01\x040\x06\x02\x01\f\x02\x01\x0E0\x06\x02\x01\n\x02"\
    "\x01\b"
  end

  def test_to_der
    der = PathgraphEncoding::PublicKey::to_der(@public_key)

    assert_equal 48, der.size
    assert_equal @der_public_key, der
  end

  def test_from_der
    key = PathgraphEncoding::PublicKey::from_der(@der_public_key)
    exp = {version: PathgraphEncoding::VERSION,
      n: 4,
      sigma: [[0,2],[6,4],[12,14],[10,8]]}
    assert_equal key, exp
  end

  def test_is_valid
    assert PathgraphEncoding::PublicKey::is_valid?(@public_key)
  end

end
