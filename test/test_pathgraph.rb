require 'minitest/autorun'
require 'pathgraph_encoding'

class TestPathGraph < Minitest::Test

  def setup
    @k = 4
    @m = 4
    @pi = [[0,1,3,2],[6,7,5,4],[12,13,15,14],[10,11,9,8]]
    @der = "0@\x02\x01\x04\x02\x01\x04080\f\x02\x01\x00\x02\x01"\
    "\x01\x02\x01\x03\x02\x01\x020\f\x02\x01\x06\x02\x01\a\x02\x01"\
    "\x05\x02\x01\x040\f\x02\x01\f\x02\x01\r\x02\x01\x0F\x02\x01"\
    "\x0E0\f\x02\x01\n\x02\x01\v\x02\x01\t\x02\x01\b"
  end

  def test_pack
    der = PathgraphEncoding.pack(@k, @m, @pi)
    assert_equal 66, der.size
    assert_equal @der, der
  end

  def test_unpack
    tuple = PathgraphEncoding.unpack(@der)
    assert_equal [@k,@m,@pi], tuple
  end

end
